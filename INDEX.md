# fdxms286

Replacement XMS driver for '286 systems or better.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FDXMS286.LSM

<table>
<tr><td>title</td><td>fdxms286</td></tr>
<tr><td>version</td><td>0.03.Temperaments (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2005-05-28</td></tr>
<tr><td>description</td><td>Replacement XMS driver for '286 systems or better.</td></tr>
<tr><td>keywords</td><td>freedos, xms, himem</td></tr>
<tr><td>author</td><td>Till Gerken &lt;till@tantalo.net&gt;, Martin Strimberg &lt;ams@ludd.ltu.se&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Martin Strimberg &lt;ams@ludd.ltu.se&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://www.ludd.ltu.se/~ams/freedos/</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.ludd.ltu.se/~ams/freedos/</td></tr>
<tr><td>platforms</td><td>dos (ASM)</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
